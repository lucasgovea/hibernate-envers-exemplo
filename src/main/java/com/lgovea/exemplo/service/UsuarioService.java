package com.lgovea.exemplo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lgovea.exemplo.domain.Usuario;
import com.lgovea.exemplo.repository.IUsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	IUsuarioRepository usuarioRepository;
	
	public List<Usuario> obterTodos() {
		return usuarioRepository.findAll();
	}
}
