package com.lgovea.exemplo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lgovea.exemplo.domain.Endereco;
import com.lgovea.exemplo.repository.IEnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	IEnderecoRepository enderecoRepository;
	
	public void removerEndereco(Long id) {
		enderecoRepository.deleteById(id);
	}
	
	public Optional<Endereco> buscarPorId(Long id) {
		return enderecoRepository.findById(id);
	}

	public Endereco atualizar(Endereco endereco) {
		return enderecoRepository.saveAndFlush(endereco);
	}

	public List<Endereco> obterTodos() {
		return enderecoRepository.findAll();
	}
}
