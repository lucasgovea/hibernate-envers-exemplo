package com.lgovea.exemplo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lgovea.exemplo.domain.Endereco;

@Repository
public interface IEnderecoRepository extends JpaRepository<Endereco, Long>{

	
}
