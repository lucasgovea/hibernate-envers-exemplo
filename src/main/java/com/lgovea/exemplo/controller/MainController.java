package com.lgovea.exemplo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lgovea.exemplo.domain.Endereco;
import com.lgovea.exemplo.service.EnderecoService;

@RestController
public class MainController {

	@Autowired
	public EnderecoService enderecoService;
	
	@ResponseBody
	@DeleteMapping("/endereco/{idEndereco}")
	public ResponseEntity<String> removerEndereco(@PathVariable Long idEndereco){
		
		enderecoService.buscarPorId(idEndereco).orElseThrow(() -> new IllegalArgumentException("User not found :: " + idEndereco));
		
		enderecoService.removerEndereco(idEndereco);
		return ResponseEntity.status(HttpStatus.OK).body("Excluido com sucesso!");
	}
	
	@ResponseBody
	@PutMapping("/endereco/{idEndereco}")
	public ResponseEntity<Endereco> alterarEndereco(@PathVariable Long idEndereco, @RequestBody Endereco enderecoParam){
		
		Endereco endereco = enderecoService.buscarPorId(idEndereco)
				.orElseThrow(() -> new IllegalArgumentException("User not found :: " + idEndereco));
		
		endereco.setBairro(enderecoParam.getBairro());
		
		endereco = enderecoService.atualizar(endereco);
		return ResponseEntity.status(HttpStatus.OK).body(endereco);
	}
	
	@ResponseBody
	@GetMapping("/endereco")
	public ResponseEntity<List<Endereco>> obterTodos(){
		
		List<Endereco> enderecos = enderecoService.obterTodos();
		
		return ResponseEntity.status(HttpStatus.OK).body(enderecos);
	}
}
