
INSERT INTO usuario(nome, profissao, data_nascimento) VALUES ('João Pedro', 'Empresário', parsedatetime( '10/10/1990', 'dd/mm/yyyy'));
INSERT INTO usuario(nome, profissao, data_nascimento) VALUES ('Luis Felipe', 'Analista de Sistemas', parsedatetime( '10/07/1970', 'dd/mm/yyyy'));
INSERT INTO usuario(nome, profissao, data_nascimento) VALUES ('Lucas', 'Dentista', parsedatetime( '29/05/1985', 'dd/mm/yyyy'));
INSERT INTO usuario(nome, profissao, data_nascimento) VALUES ('Felipe', 'Médico', parsedatetime( '31/01/1995', 'dd/mm/yyyy'));

insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua das Flores', 10, 'Centro', 'RJ', 'Rio de Janeiro', 'loja 4', 1);
insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua dos Carros', 4234, 'Centro', 'MG', 'Belo Horizonte', 'casa', 1);
insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua da Praia', 1000, 'Boa Viagem', 'PE', 'Recife', 'loja 10', 1);
insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua dos Predios', null, 'Praia do Canto', 'ES', 'Vitória', 'casa', 2);
insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua das Pedras', 568, 'Centro', 'RJ', 'Rio de Janeiro', 'loja 2', 3);
insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua das Casas', 435, 'Lapa', 'SP', 'São Paulo', 'ap 201', 3);
insert into endereco(logradouro, numero, bairro, estado, cidade, complemento, id_usuario) VALUES ('Rua dos Bares', 51, 'Lapa', 'RJ', 'Rio de Janeiro', 'casa', 4);
